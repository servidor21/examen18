<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{

    public function books()
    {
        return $this->hasMany('App\Book');
        //un genero tiene muchos libros (1:n)
    }
}
