<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    protected $fillable = ['name', 'country', 'birth_year'];

    public function books()
    {
        return $this->belongsToMany('App\Book');

        //muchos autores tienen muchos libros (n:m)
    }
}
