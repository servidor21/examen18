<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'pages', 'year', 'gender_id', 'user_id'];

    public function gender()
    {
        return $this->belongsTo('App\Gender');

        //muchos libros pertenecen a un genero (n:1)
    }

    public function authors()
    {
        return $this->belongsToMany('App\Author');

        //muchos libros tienen muchos autores (n:m)
    }
}
