@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de libros
</h1>

<ul>
    <li>Título: {{ $book->title }}</li>
    <li>Páginas: {{ $book->pages }}</li>
    <li>Año: {{ $book->year }}</li>
    <li>Género: {{ $book->gender->name }}</li>
    <li>Usuario alta: {{ $book->user->name }}</li>
</ul>

<p><a href="/books/{{ $book->id }}/edit">Modificar</a></p>
</div>
@endsection
