@extends('layouts.app')

@section('content')
    <h1>Lista de libros</h1>
    @can('create', 'App\Book')
        <a href="/books/create">Nuevo</a>
    @else
        No puedes dar altas!!
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Titulo</th>
                <th>Paginas</th>
                <th>Año</th>
                <th>Genero</th>
                <th>Usuario alta</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($books as $book)
            <tr>
                <td>  {{ $book->id }} </td>
                <td>  {{ $book->title }} </td>
                <td>  {{ $book->pages }} </td>
                <td>  {{ $book->year }} </td>
                <td>  {{ $book->gender_id }} </td>
                <td>  {{ $book->{{user_id->name}} }} </td>
                <td>  
                    <form method="post" action="/books/{{ $book->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $book)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $book)
                        <a href="/books/{{ $book->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $book)
                        <a href="/books/{{ $book->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $books->render() }}

@endsection('content')