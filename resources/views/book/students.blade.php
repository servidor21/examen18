@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudios
</h1>

<ul>
    <li>Codigo: {{ $study->code }}</li>
    <li>Abreviatura: {{ $study->abreviation }}</li>
    <li>Nombre: {{ $study->name }}</li>
    <li>Nombre Corto: {{ $study->shortName }}</li>
    <li>Nivel del estudio: {{ $study->level->name }}</li>
</ul>

<p><a href="/studies/{{ $study->id }}/edit">Modificar</a></p>

    <h3>Estudiantes del estudio actual</h3>
    <ol>
        @foreach ($study->students as $student)
        <li>{{ $student->dni }} - {{ $student->firstname }} - {{ $student->lastname }} - Curso  {{ $student->pivot->course }}º - Año {{ $student->pivot->year }}</li>
        @endforeach
    </ol>
</div>
@endsection
