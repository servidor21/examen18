@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudios
</h1>

<div class="form">
<form action="/studies" method="post">
    {{ csrf_field() }}

    <div class="form-group">
        <label>Código: </label>
        <input type="text" name="code" value="{{ old('code') }}">
        <div>
            {{ $errors->first('code') }}
        </div>
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name') }}">
        <div>
            {{ $errors->first('name') }}
        </div>
    </div>

    <div class="form-group">
        <label>Nombre Corto: </label>
        <input type="text" name="shortName" value="{{ old('shortName') }}">
        <div>
            {{ $errors->first('shortName') }}
        </div>
    </div>

    <div class="form-group">
        <label>Abreviatura: </label>
        <input type="text" name="abreviation" value="{{ old('abreviation') }}">
        <div>
            {{ $errors->first('abreviation') }}
        </div>
    </div>

    <div>
        <select type="select" name="level_id" value="">
            @foreach ($levels as $level)
            <option value="{{ $level->id }}">
                {{ $level->name }}
            </option>
            @endforeach                
        </select>        
    </div>

    <div>
        <select type="select" name="family_id" value="">
            @foreach ($families as $family)
            <option value="{{ $family->id }}">
                {{ $family->name }}
            </option>
            @endforeach                
        </select>        
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>    
</form>
</div>
</div>
@endsection
