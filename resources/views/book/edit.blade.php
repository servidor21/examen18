@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de libros
</h1>

<div class="form">
<form action="/books/{{ $book->id }}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}


    <div class="form-group">
        <label>Título: </label>
        <input type="text" name="title" value="{{ $book->title }}">
    </div>

    <div class="form-group">
        <label>Páginas: </label>
        <input type="text" name="pages" value="{{ $book->pages }}">
    </div>

    <div class="form-group">
        <label>Año: </label>
        <input type="text" name="year" value="{{ $book->year }}">
    </div>

    <div class="form-group">
        <label>Abreviatura: </label>
        <input type="text" name="abreviation" value="{{ $book->abreviation }}">
    </div>

    <div class="form-group">
        <label>Nivel del estudio: </label>

        <select type="select" name="level_id">
            @foreach ($levels as $level)
            <option value="{{ $level->id }}"
                @if ($level->id == old('level_id',  $study->level_id)) 
                    selected 
                @endif >
                {{ $level->name }}
            </option>
            @endforeach                
        </select>

    </div>
    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>    
</form>
</div>
</div>
@endsection
